package br.com.mastertech.delivery.models;

import org.hibernate.validator.constraints.br.CNPJ;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Restaurante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser vazio")
    private String nome;

    @CNPJ
    private String cnpj;

    @NotNull(message = "nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser vazio")
    private String endereco;

    @NotNull(message = "nome não pode ser nulo")
    @NotBlank(message = "Nome não pode ser vazio")
    private String telefone;

    @OneToMany
    private List<Produto> produtos;

    @OneToMany
    private List<Pedido> pedidos;


    public Restaurante() {
    }


}
