package br.com.mastertech.delivery.auth;

import br.com.mastertech.delivery.services.ClienteService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroDeAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;
    private ClienteService clienteService;

    public FiltroDeAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil, ClienteService clienteService) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.clienteService = clienteService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String authorizationHeader = request.getHeader("Authorization");

        if(authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            String tokenLimpo = authorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken authToken = getAutenticacao(request, tokenLimpo);

            if(authToken != null){
                SecurityContextHolder.getContext().setAuthentication(authToken);
            }
        }
        chain.doFilter(request, response);
    }

    private UsernamePasswordAuthenticationToken getAutenticacao(HttpServletRequest request, String token){
        if(jwtUtil.tokenValido(token)){
            String cpf = jwtUtil.getCpf(token);
            UserDetails cliente = clienteService.loadUserByUsername(cpf);
            return new UsernamePasswordAuthenticationToken(cliente, null, cliente.getAuthorities());
        }else{
            return null;
        }
    }

}
