package br.com.mastertech.delivery.auth;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTUtil {

    private static final String SEGREDO = "dlgsa49awc8a15dc96pm16a";

    private static final Long TEMPO_DE_VALIDADE = 600000l;

    //Metodo para gerar o token
    public String gerarToken(String cpf){
        Date dataDeVencimento = new Date(System.currentTimeMillis()+TEMPO_DE_VALIDADE);

        String token = Jwts.builder().setSubject(cpf)
                .setExpiration(dataDeVencimento)
                .signWith(SignatureAlgorithm.ES512.HS512, SEGREDO.getBytes()).compact();
        return token;
    }

    //Metodos para validação do token
    public String getCpf(String token){
        Claims claims = getClaims(token);
        String cpf = claims.getSubject();
        return cpf;
    }

    public Claims getClaims (String token){
        return Jwts.parser().setSigningKey(SEGREDO.getBytes()).parseClaimsJws(token).getBody();
    }

    public boolean tokenValido(String token){
        Claims claims = getClaims(token);
        String cpf = claims.getSubject();

        Date dataDeExpiracao = claims.getExpiration();
        Date dataAtual = new Date(System.currentTimeMillis());

        if (cpf != null && dataDeExpiracao != null && dataAtual.before(dataDeExpiracao)){
            return true;
        }else{
            return false;
        }
    }



}
