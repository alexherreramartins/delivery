package br.com.mastertech.delivery.DTOs;

public class LoginClienteDTO {

    private String cpf;
    private String senha;

    public LoginClienteDTO() {
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

}
